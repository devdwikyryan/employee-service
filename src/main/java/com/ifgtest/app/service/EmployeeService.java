package com.ifgtest.app.service;

import java.util.List;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import com.ifgtest.app.dto.LoginRequest;
import com.ifgtest.app.integration.ServiceClient;
import com.ifgtest.app.model.Employee;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;

@ApplicationScoped
public class EmployeeService {

    @Inject
    @Channel("employee-events")
    Emitter<Boolean> emitter;

    @Inject
    @RestClient
    ServiceClient serviceClient;

    public Response login(LoginRequest loginRequest) {
        if (loginRequest.getUsername().equals("admin") && loginRequest.getPassword().equals("password")) {
            System.out.println("Sending message....");
            emitter.send(true);
            return Response.ok().build();
        } else {
            emitter.send(false);
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    public List<Employee> getEmployeeList() {
        return serviceClient.getEmployeeList();
    }
}
