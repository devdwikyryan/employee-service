package com.ifgtest.app.integration;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import com.ifgtest.app.model.Employee;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@ApplicationScoped
@Path("/")
@RegisterRestClient(baseUri = "http://localhost:8081")
public interface ServiceClient {
    @GET
    @Path("/employee-list")
    @Produces(MediaType.APPLICATION_JSON)
    List<Employee> getEmployeeList();
}
