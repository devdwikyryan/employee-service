package com.ifgtest.app;

import java.io.StringWriter;
import java.util.List;

import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;

import com.ifgtest.app.dto.LoginRequest;
import com.ifgtest.app.model.Employee;
import com.ifgtest.app.service.EmployeeService;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/")
public class EmployeeResource {

    @Inject
    EmployeeService employeeService;

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(LoginRequest loginRequest) {
        return employeeService.login(loginRequest);
    }

    @GET
    @Path("/employee-list")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employee> getEmployeeList() {
        return employeeService.getEmployeeList();
    }

    @GET
    @Path("/export-to-csv")
    @Produces(MediaType.TEXT_PLAIN)
    @APIResponses(value = {
            @APIResponse(responseCode = "200", description = "CSV exported successfully"),
            @APIResponse(responseCode = "500", description = "Internal server error")
    })
    public Response exportToCsv() {
        try {
            List<Employee> dataList = employeeService.getEmployeeList();
            StringWriter writer = new StringWriter();
            writer.append("id,name,age,department\n");

            for (var data : dataList) {
                writer.append(Long.toString(data.getId()))
                        .append(",")
                        .append(data.getName())
                        .append(",")
                        .append(Integer.toString(data.getAge()))
                        .append(",")
                        .append(data.getDepartment())
                        .append("\n");
            }

            return Response.ok(writer.toString())
                    .header("Content-Disposition", "attachment; employee-list.csv")
                    .build();
        } catch (Exception e) {
            return Response.serverError().entity("Failed to export data to CSV: " + e.getMessage()).build();
        }
    }
}
